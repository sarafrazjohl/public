#!/usr/bin/env bash

#To install run:
# bash <(curl -s -H 'Cache-Control: no-cache' https://bitbucket.org/sarafrazjohl/public/raw/HEAD/jbox.sh)

#Basic setup
yum clean all
yum -y update
yum -y install vim git

#OhMyZsh
export FILE_ZSHRC="~/.zshrc"
export FILE_VIMRC="~/.vimrc"
yum install -y zsh
echo "vagrant" | sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh
sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="steeef"/g' $FILE_ZSHRC
sed -i 's/# DISABLE_AUTO_UPDATE/DISABLE_AUTO_UPDATE/g' $FILE_ZSHRC
touch $FILE_VIMRC
echo 'syntax on' >> $FILE_VIMRC
