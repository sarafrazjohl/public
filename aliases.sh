#!/usr/bin/env bash

#To install run:
#bash <(curl -s -H 'Cache-Control: no-cache' https://bitbucket.org/sarafrazjohl/public/raw/HEAD/aliases.sh)

#Remove existing file
rm -f ~/.johl_aliases.sh

#Git shortcuts
echo "alias ga='git add'" >> ~/.johl_aliases.sh
echo "alias gau='git add -u'" >> ~/.johl_aliases.sh

#Essentials
echo "alias ls='ls -lA'" >> ~/.johl_aliases.sh
echo "alias ll='ls -lhAG'" >> ~/.johl_aliases.sh

#Timesavers
echo "alias rc='vim ~/.zshrc'" >> ~/.johl_aliases.sh
#echo "alias svc='bash -xc '\''sudo systemctl \$1 \$0'\''' #service #action" >> ~/.johl_aliases.sh
echo "alias f5='sudo systemctl restart httpd; sudo systemctl restart mysqld'" >> ~/.johl_aliases.sh
echo "alias debug=\"echo 'Httpd Status'; sudo service httpd status; echo 'Mysqld Status'; sudo service mysqld status; echo 'Httpd error log'; sudo tail /var/log/httpd/error_log\"" >> ~/.johl_aliases.sh
echo "alias cdw='cd /usr/local/www && ll'" >> ~/.johl_aliases.sh

#Git
echo "alias ga='git add' #file" >> ~/.johl_aliases.sh
echo "alias gau='git add -u'" >> ~/.johl_aliases.sh
echo "alias gaa='git add --all'" >> ~/.johl_aliases.sh
echo "alias gb='git branch -m' #branch_name" >> ~/.johl_aliases.sh
echo "alias gbu='git branch --set-upstream-to=origin/\$(git symbolic-ref --short HEAD) \$(git symbolic-ref --short HEAD)'" >> ~/.johl_aliases.sh
echo "alias gca='git commit --amend -m' #message" >> ~/.johl_aliases.sh
echo "alias gcm='git commit -m' #message" >> ~/.johl_aliases.sh
echo "alias gci='git clean -i'" >> ~/.johl_aliases.sh
echo "alias gco='git checkout' #branch_name" >> ~/.johl_aliases.sh
echo "alias gfe='git for-each-ref --sort=-committerdate refs/heads/'" >> ~/.johl_aliases.sh
echo "alias gi='git check-ignore'" >> ~/.johl_aliases.sh
echo "alias giv='git check-ignore'" >> ~/.johl_aliases.sh
echo "alias gia='git check-ignore **/*'" >> ~/.johl_aliases.sh
echo $"alias gl='git log --pretty=format:\"%C(yellow)%h %Cred%ad %Cblue%an%Cgreen%d %Creset%s\" --date=short'" >> ~/.johl_aliases.sh
echo "alias glg='git log --graph --oneline --decorate --max-count=30'" >> ~/.johl_aliases.sh
echo "alias gp='git push -u origin \$(git rev-parse --abbrev-ref HEAD)'" >> ~/.johl_aliases.sh
echo "alias gpf='git push -f origin \$(git rev-parse --abbrev-ref HEAD)'" >> ~/.johl_aliases.sh
echo "alias gr='git reset'" >> ~/.johl_aliases.sh
echo "alias grl='git reflog'" >> ~/.johl_aliases.sh
echo "alias gs='git status'" >> ~/.johl_aliases.sh

#Docker
echo "alias dp='docker ps -a'" >> ~/.johl_aliases.sh
echo "alias di='docker images -a'" >> ~/.johl_aliases.sh
echo "alias dn='docker network ls'" >> ~/.johl_aliases.sh
echo "alias de='docker exec -it' #container #command" >> ~/.johl_aliases.sh

#Vagrant
echo "alias vup='vagrant up'" >> ~/.johl_aliases.sh
echo "alias vssh='vagrant ssh'" >> ~/.johl_aliases.sh
echo "alias vhalt='vagrant halt'" >> ~/.johl_aliases.sh

#Add to rc file
chmod u+x ~/.johl_aliases.sh
if [ "$(uname)" == "Darwin" ]; then
    sed -i '' '/johl_aliases/d' ~/.zshrc
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    sed -i '/johl_aliases/d' ~/.zshrc
fi
echo "source ~/.johl_aliases.sh" >> ~/.zshrc

#Setup color output for git
git config --global color.branch auto
git config --global color.diff auto
git config --global color.status auto
git config --global color.ui auto
git config --global core.editor vim

#Relaod aliases
. ~/.johl_aliases.sh